/* eslint-disable */
import * as BABYLON from 'babylonjs'
import 'babylonjs-loaders';

var canvas = document.getElementById('renderCanvas')
canvas.width = window.innerWidth
canvas.height = window.innerHeight
var engine = new BABYLON.Engine(canvas, true, {preserveDrawingBuffer: true, stencil: true})

var createScene = function () {
	var scene = new BABYLON.Scene(engine)
	var camera = new BABYLON.ArcRotateCamera('Camera', -Math.PI / 1, Math.PI / 1, 10, BABYLON.Vector3.Zero(), scene)
	camera.wheelPrecision = 50;
	camera.attachControl(canvas, true)
	camera.minZ = 1

	// Environment Texture
	var hdrTexture = new BABYLON.HDRCubeTexture('./../../static/textures/kitchen.hdr', scene, 512)

	// scene.imageProcessingConfiguration.exposure = 0.1
	// scene.imageProcessingConfiguration.contrast = 1.6
	// scene.ambientColor = new BABYLON.Color3(0, 1, 1);

	// Skybox
	// var hdrSkybox = BABYLON.Mesh.CreateSphere('hdrSkyBox', 0, 1000, scene)
	// var hdrSkyboxMaterial = new BABYLON.PBRMaterial('skyBox', scene)
	// hdrSkyboxMaterial.backFaceCulling = false
	// hdrSkyboxMaterial.reflectionTexture = hdrTexture.clone()
	// hdrSkyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE
	// hdrSkyboxMaterial.microSurface = 1.0
	// hdrSkyboxMaterial.disableLighting = true
	// hdrSkybox.material = hdrSkyboxMaterial
	// hdrSkybox.infiniteDistance = true

	// Create meshes
	// var spherePlastic = BABYLON.Mesh.CreateSphere('spherePlastic', 48, 30.0, scene)
	// spherePlastic.translate(new BABYLON.Vector3(0, 0, 2), -60)

	// Create materials
	var glass = new BABYLON.PBRMaterial('glass', scene)
	glass.reflectionTexture = hdrTexture
	glass.refractionTexture = hdrTexture
	glass.linkRefractionWithTransparency = true
	glass.indexOfRefraction = 0.52
	glass.alpha = 0
	glass.microSurface = 1
	glass.reflectivityColor = new BABYLON.Color3(0.2, 0.2, 0.2)
	glass.albedoColor = new BABYLON.Color3(0.85, 0.85, 0.85)
	// //sphereGlass.material = glass

	var metal = new BABYLON.PBRMaterial('metal', scene)
	metal.reflectionTexture = hdrTexture
	metal.microSurface = 0.96
	metal.reflectivityColor = new BABYLON.Color3(0.85, 0.85, 0.85)
	metal.albedoColor = new BABYLON.Color3(0.01, 0.01, 0.01)
	// //sphereMetal.material = metal

	var plastic = new BABYLON.PBRMaterial('plastic', scene)
	plastic.reflectionTexture = hdrTexture
	plastic.reflectionTexture.coordinatesMode = BABYLON.Texture.SPHERICAL_MODE  ;
	plastic.microSurface = 1
	plastic.albedoColor = new BABYLON.Color3(0.5, 0.5, 0.5)
	plastic.reflectivityColor = new BABYLON.Color3(0.05, 0.05, 0.05)
	//spherePlastic.material = plastic
	
    // The first parameter can be used to specify which mesh to import. Here we import all meshes
    // BABYLON.SceneLoader.Append("./../../static/", "mug.obj", scene, function (newMeshes) {
	// 	console.log(newMeshes)
	// 	newMeshes.meshes[0].material = plastic
	// 	newMeshes.meshes[1].material = plastic
	// 	newMeshes.meshes[2].material = plastic
	// });

	// BABYLON.SceneLoader.ImportMesh("", "./../../static/ak47/", "ak47.gltf", scene, function (newMeshes) {
	// 	camera.target = newMeshes[0]
	// 	console.log(newMeshes[0])
	// 	//var e = scene.createDefaultEnvironment();
	// 	var envTexture = new BABYLON.HDRCubeTexture('./../../static/textures/kitchen.hdr', scene, 512)
	// 	scene.createDefaultSkybox(envTexture, true, 1000);

	// 	scene.imageProcessingConfiguration.exposure = 0.5
	// 	scene.imageProcessingConfiguration.contrast = 1
	// });

	BABYLON.SceneLoader.ImportMesh("", "./../../static/bottle/", "WaterBottle.gltf", scene, function (newMeshes) {

		var obj = newMeshes[1];
        
		// var pbr = new BABYLON.PBRMetallicRoughnessMaterial("pbr", scene);
		// pbr.diffuseTexture = new BABYLON.Texture("./../../static/bottle/WaterBottle_baseColor2.png", scene);
		
		//obj.material.albedoTexture = new BABYLON.Texture("./../../static/bottle/WaterBottle_baseColor2.png", scene, true, false);
		var newAlbedo = new BABYLON.DynamicTexture("texture0", 2048, scene);
		newAlbedo.url = "https://cdn.pbrd.co/images/HvfjFoc.png";
		
		// obj.material.albedoTexture.url = "https://cdn.pbrd.co/images/HvfjFoc.png";
	    obj.material.albedoTexture.update();
		console.log(obj.material)

		camera.target = newMeshes[0]
		newMeshes[0].scaling.y = 10;
		newMeshes[0].scaling.x = 10;
		newMeshes[0].scaling.z = 10;

		//var e = scene.createDefaultEnvironment();
		var envTexture = new BABYLON.HDRCubeTexture('./../../static/textures/kitchen.hdr', scene, 512)
		scene.createDefaultSkybox(envTexture, true, 1000);

		scene.imageProcessingConfiguration.exposure = 0.5
		scene.imageProcessingConfiguration.contrast = 1

	});	

	// BABYLON.SceneLoader.ImportMesh("", "https://www.babylonjs.com/Assets/DamagedHelmet/glTF/", "DamagedHelmet.gltf", scene, function (newMeshes) {
    //     camera.target = newMeshes[0]
    //     var e = scene.createDefaultEnvironment();
    //     // Creating default environment enables tone mapping so disable for demo
    // });

	return scene
}
var scene = createScene()

engine.runRenderLoop(function () {
	scene.render()
})

window.addEventListener('resize', function () {
	canvas.width = window.innerWidth
	canvas.height = window.innerHeight
	engine.resize()
})