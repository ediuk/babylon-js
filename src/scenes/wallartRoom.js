import * as BABYLON from 'babylonjs'
import 'babylonjs-loaders'

export const wallartRoomScene = {
  data () {
    return {
      wallartRoom: {
        photoNum: 1
      }
    }
  },
  methods: {
    nextWallartRoomUserPhoto () {
      var pbr = this.scene.meshes[1].material
      pbr.albedoTexture = new BABYLON.Texture('./../../static/wallart_room/photo' + this.wallartRoom.photoNum + '.jpg', this.scene)
      pbr.albedoTexture.coordinatesIndex = 1
      this.wallartRoom.photoNum++
      if (this.wallartRoom.photoNum === 5) { this.wallartRoom.photoNum = 0 }
    },
    applyWallartRoomScene () {
      var scene = new BABYLON.Scene(this.engine)
      scene.ambientColor = new BABYLON.Color3(1, 1, 1)

      var camera = new BABYLON.ArcRotateCamera('Camera', -Math.PI / 1, Math.PI / 3.5, 10, BABYLON.Vector3.Zero(), scene)
      camera.panningSensibility = 100
      camera.lowerRadiusLimit = 75
      camera.upperRadiusLimit = 200
      camera.lowerAlphaLimit = -Math.PI / 1.2
      camera.upperAlphaLimit = -Math.PI / 6
      camera.lowerBetaLimit = Math.PI / 5
      camera.upperBetaLimit = Math.PI / 1.6
      camera.attachControl(this.canvas, true)

      var pbrRoom = new BABYLON.PBRMaterial('pbrRoom', scene)
      pbrRoom.albedoTexture = new BABYLON.Texture('./../../static/wallart_room/room_diffuse.jpg', scene)
      pbrRoom.ambientColor = new BABYLON.Color3(1, 1, 1)
      pbrRoom.metallic = 0
      pbrRoom.roughness = 1
      pbrRoom.backFaceCulling = true

      var pbrSofa = new BABYLON.PBRMaterial('pbrSofa', scene)
      pbrSofa.albedoTexture = new BABYLON.Texture('./../../static/wallart_room/sofa_diffuse.png', scene)
      pbrSofa.ambientColor = new BABYLON.Color3(1, 1, 1)
      pbrSofa.metallic = 0
      pbrSofa.roughness = 1
      pbrSofa.backFaceCulling = false

      var pbrCanvas = new BABYLON.PBRMaterial('pbrCanvas', scene)
      pbrCanvas.albedoTexture = new BABYLON.Texture('./../../static/wallart_room/photo0.jpg', scene)
      pbrCanvas.albedoTexture.coordinatesIndex = 1
      pbrCanvas.ambientTexture = new BABYLON.Texture('./../../static/wallart_room/canvas_ao.png', scene)
      pbrCanvas.lightmapTexture = new BABYLON.Texture('./../../static/wallart_room/canvas_light.png', scene)
      pbrCanvas.lightmapTexture.level = 0.05
      pbrCanvas.ambientColor = new BABYLON.Color3(1, 1, 1)
      pbrCanvas.metallic = 0
      pbrCanvas.roughness = 1
      pbrCanvas.backFaceCulling = false

      BABYLON.SceneLoader.ImportMesh('', './../../static/wallart_room/', 'wallart_room.babylon', scene, function (newMeshes) {
        camera.setTarget(new BABYLON.Vector3(newMeshes[1].position.x, newMeshes[1].position.y + 10, newMeshes[1].position.z))
        camera.beta = Math.PI / 2
        camera.alpha = -Math.PI / 2
        camera.radius = 125

        const room = newMeshes[2]
        const sofa = newMeshes[0]
        const canvas = newMeshes[1]

        room.material = pbrRoom
        sofa.material = pbrSofa
        canvas.material = pbrCanvas

        scene.imageProcessingConfiguration.exposure = 1
        scene.imageProcessingConfiguration.contrast = 1
      })
      this.scene = scene
    }
  }
}
