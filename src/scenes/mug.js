import * as BABYLON from 'babylonjs'
import 'babylonjs-loaders'

export const mugScene = {
  data () {
    return {
      mug: {
        photoNum: 0
      }
    }
  },
  methods: {
    randomizeMugColor () {
      var pbr = this.scene.meshes[3].material
      pbr.albedoColor = new BABYLON.Color3(Math.random(), Math.random(), Math.random())
    },
    nextMugUserPhoto () {
      var pbr = this.scene.meshes[2].material
      pbr.albedoTexture = new BABYLON.Texture('./../../static/mug/photo' + (this.mug.photoNum) + '.jpg', this.scene)
      pbr.albedoTexture.vScale = -1
      this.mug.photoNum++
      if (this.mug.photoNum === 8) { this.mug.photoNum = 0 }
    },
    applyMugScene (envKey, envRes) {
      var scene = new BABYLON.Scene(this.engine)
      var camera = new BABYLON.ArcRotateCamera('Camera', -Math.PI / 1, Math.PI / 3.5, 10, BABYLON.Vector3.Zero(), scene)
      camera.useAutoRotationBehavior = true
      camera.autoRotationBehavior.idleRotationSpeed = 0.2
      camera.autoRotationBehavior.idleRotationWaitTime = 1000
      camera.autoRotationBehavior.idleRotationSpinupTime = 2000
      camera.autoRotationBehavior.zoomStopsAnimation = true
      camera.wheelPrecision = 50
      camera.lowerRadiusLimit = 5
      camera.upperRadiusLimit = 10
      camera.attachControl(this.canvas, true)

      var pbr1 = new BABYLON.PBRMaterial('pbr', scene)
      pbr1.albedoColor = new BABYLON.Color3(1, 1, 1)
      pbr1.metallic = 0.2
      pbr1.roughness = 0.1
      pbr1.microSurface = 0 // Let the texture controls the value
      pbr1.backFaceCulling = false

      var pbr2 = new BABYLON.PBRMaterial('pbr2', scene)
      pbr2.albedoTexture = new BABYLON.Texture('./../../static/mug/photo0.jpg')
      pbr2.albedoTexture.vScale = -1
      pbr2.albedoColor = new BABYLON.Color3(1, 1, 1)
      pbr2.metallic = 0.2
      pbr2.roughness = 0.1
      pbr2.microSurface = 0 // Let the texture controls the value
      pbr2.backFaceCulling = false

      var pbr3 = new BABYLON.PBRMaterial('pbr3', scene)
      pbr3.albedoColor = new BABYLON.Color3(0, 0, 1)
      pbr3.metallic = 0.2
      pbr3.roughness = 0.1
      pbr3.microSurface = 0 // Let the texture controls the value
      pbr3.backFaceCulling = false

      var that = this

      BABYLON.SceneLoader.ImportMesh('', './../../static/mug/', 'mug_superdetached.gltf', scene, function (newMeshes) {
        newMeshes[1].material = pbr1
        newMeshes[1].scaling.y = 0.05
        newMeshes[1].scaling.x = 0.05
        newMeshes[1].scaling.z = 0.05
        newMeshes[1].rotation.y = 2

        newMeshes[2].material = pbr2
        newMeshes[2].scaling.y = 0.05
        newMeshes[2].scaling.x = 0.05
        newMeshes[2].scaling.z = 0.05
        newMeshes[2].rotation.y = 2

        newMeshes[3].material = pbr3
        newMeshes[3].scaling.y = 0.05
        newMeshes[3].scaling.x = 0.05
        newMeshes[3].scaling.z = 0.05
        newMeshes[3].rotation.y = 2

        camera.target = newMeshes[1]
        camera.beta = Math.PI / 2.5
        camera.alpha = -Math.PI / 3.5

        scene.imageProcessingConfiguration.exposure = 0.3
        scene.imageProcessingConfiguration.contrast = 1

        that.applyEnv(scene, envKey, envRes)
      })
      this.scene = scene
    }
  }
}
