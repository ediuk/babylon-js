import * as BABYLON from 'babylonjs'
import 'babylonjs-loaders'

export const canvasSpotlightScene = {
  data () {
    return {
      canvasSpotlight: {
        photoNum: 0
      }
    }
  },
  methods: {
    applyCanvasSpotlightScene () {
      var scene = new BABYLON.Scene(this.engine)
      scene.ambientColor = new BABYLON.Color3(1, 1, 1)
      scene.clearColor = new BABYLON.Color3(0, 0, 0)

      var camera = new BABYLON.ArcRotateCamera('Camera', -Math.PI / 1, Math.PI / 3.5, 10, BABYLON.Vector3.Zero(), scene)
      camera.panningSensibility = 100
      camera.attachControl(this.canvas, true)

      var pbrWood = new BABYLON.PBRMaterial('pbrWood', scene)
      pbrWood.albedoTexture = new BABYLON.Texture('./../../static/canvas30x30/wood004x16.jpg', scene)
      pbrWood.bumpTexture = new BABYLON.Texture('./../../static/canvas30x30/wood004x16b.jpg', scene)
      pbrWood.ambientTexture = new BABYLON.Texture('./../../static/canvas30x30/HolzCompleteMap.jpg', scene)
      pbrWood.ambientColor = new BABYLON.Color3(1, 1, 1)
      pbrWood.metallic = 0
      pbrWood.roughness = 1
      pbrWood.backFaceCulling = false

      var pbrPhoto = new BABYLON.PBRMaterial('pbrPhoto', scene)
      pbrPhoto.albedoTexture = new BABYLON.Texture('./../../static/canvas30x30/photo0.jpg', scene)
      pbrPhoto.albedoColor = new BABYLON.Color3(1, 1, 1)
      pbrPhoto.ambientTexture = new BABYLON.Texture('./../../static/canvas30x30/canvasAmbientBumpMap.jpg', scene)
      pbrPhoto.ambientColor = new BABYLON.Color3(1, 1, 1)
      pbrPhoto.metallic = 0
      pbrPhoto.roughness = 1
      pbrPhoto.backFaceCulling = false

      var pbrBackside = new BABYLON.PBRMaterial('pbrBackside', scene)
      pbrBackside.albedoColor = new BABYLON.Color3(1, 1, 1)
      pbrBackside.ambientTexture = new BABYLON.Texture('./../../static/canvas30x30/backsideCompleteMap.jpg', scene)
      pbrBackside.ambientColor = new BABYLON.Color3(1, 1, 1)
      pbrBackside.metallic = 0
      pbrBackside.roughness = 1
      pbrBackside.backFaceCulling = false

      var pbrFloor = new BABYLON.PBRMaterial('pbrFloor', scene)
      pbrFloor.albedoTexture = new BABYLON.Texture('./../../static/canvas30x30/floorCompleteMap.jpg', scene)
      pbrFloor.albedoColor = new BABYLON.Color3(1, 1, 1)
      pbrFloor.ambientColor = new BABYLON.Color3(1, 1, 1)
      pbrFloor.metallic = 0
      pbrFloor.roughness = 1
      pbrFloor.backFaceCulling = true

      BABYLON.SceneLoader.ImportMesh('', './../../static/canvas30x30/', 'canvas30x30.babylon', scene, function (newMeshes) {
        camera.setTarget(new BABYLON.Vector3(newMeshes[1].position.x, newMeshes[1].position.y + 20, newMeshes[1].position.z))
        camera.beta = Math.PI / 2
        camera.alpha = Math.PI / 1.5
        camera.radius = 70

        const holz = newMeshes[0]
        const photo = newMeshes[1]
        const backside = newMeshes[2]
        const floor = newMeshes[4]

        holz.material = pbrWood
        photo.material = pbrPhoto
        backside.material = pbrBackside
        floor.material = pbrFloor

        scene.imageProcessingConfiguration.exposure = 1
        scene.imageProcessingConfiguration.contrast = 1
      })
      this.scene = scene
    }
  }
}
