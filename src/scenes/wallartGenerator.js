import * as BABYLON from 'babylonjs'
import 'babylonjs-loaders'

export const wallartGeneratorScene = {
  data () {
    return {
      firstLoad: true,
      wallart: {
        photoNum: 0
      }
    }
  },
  methods: {
    generateNewWallart () {
      var temp = {}
      var textureW = null
      var textureH = null
      var minValueW = null
      var maxValueW = null
      var minValueH = null
      var maxValueH = null
      var photo = {}

      temp = this.scene.meshes.find(mesh => mesh.id === 'wallartObj'); if (temp) temp.dispose()
      temp = this.scene.meshes.find(mesh => mesh.id === 'photo'); if (temp) temp.dispose()
      temp = this.scene.meshes.find(mesh => mesh.id === 'wallartTop'); if (temp) temp.dispose()
      temp = this.scene.meshes.find(mesh => mesh.id === 'wallartLeft'); if (temp) temp.dispose()
      temp = this.scene.meshes.find(mesh => mesh.id === 'wallartRight'); if (temp) temp.dispose()
      temp = this.scene.meshes.find(mesh => mesh.id === 'wallartBottom'); if (temp) temp.dispose()
      temp = this.scene.meshes.find(mesh => mesh.id === 'wallartBack'); if (temp) temp.dispose()
      temp = this.scene.meshes.find(mesh => mesh.id === 'acryl'); if (temp) temp.dispose()

      if (this.wallart.type === 'canvas') {
        this.wallart.depth = this.wallart.bleed
        textureW = this.wallart.width + this.wallart.depth
        textureH = this.wallart.height + this.wallart.depth
      } else {
        this.wallart.depth = 3
        textureW = this.wallart.width + this.wallart.bleed
        textureH = this.wallart.height + this.wallart.bleed
      }

      var wallartObj = new BABYLON.Mesh.CreateBox('wallartObj', 1, this.scene)
      wallartObj.isVisible = false

      if (this.wallart.type === 'canvas') {
        minValueW = this.wallart.depth / textureW
        maxValueW = 1 - (this.wallart.depth / textureW)
        minValueH = this.wallart.depth / textureH
        maxValueH = 1 - (this.wallart.depth / textureH)

        photo = BABYLON.MeshBuilder.CreatePlane('photo',
          {
            height: this.wallart.height,
            width: this.wallart.width,
            updatable: true,
            frontUVs: new BABYLON.Vector4(minValueW, minValueH, maxValueW, maxValueH),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        photo.parent = wallartObj

        var photoBackW = 1
        var photoBackH = 1
        if (this.wallart.width >= this.wallart.height) {
          photoBackH = this.wallart.height / this.wallart.width
        } else {
          photoBackW = this.wallart.width / this.wallart.height
        }
        var photoBack = BABYLON.MeshBuilder.CreatePlane('wallartBack',
          {
            height: this.wallart.height,
            width: this.wallart.width,
            updatable: true,
            frontUVs: new BABYLON.Vector4(0, 0, photoBackW, photoBackH),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        photoBack.rotation.y = Math.PI * 3
        photoBack.position.z = this.wallart.depth
        photoBack.parent = wallartObj

        var photoTop = BABYLON.MeshBuilder.CreatePlane('wallartTop',
          {
            height: this.wallart.depth,
            width: this.wallart.width,
            updatable: true,
            // frontUVs: new BABYLON.Vector4(this.wallart.depth / textureW, 0, 1 - (this.wallart.depth / textureW), 1 - (this.wallart.depth / textureH)),
            frontUVs: new BABYLON.Vector4(minValueW, maxValueH, maxValueW, 1),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        photoTop.rotation.x = -Math.PI * 1.5
        photoTop.position.y = this.wallart.height / 2
        photoTop.position.z = this.wallart.depth / 2
        photoTop.parent = wallartObj

        var photoBottom = BABYLON.MeshBuilder.CreatePlane('wallartBottom',
          {
            height: this.wallart.depth,
            width: this.wallart.width,
            updatable: true,
            // frontUVs: new BABYLON.Vector4(this.wallart.depth / textureW, this.wallart.depth / textureH, 1 - (this.wallart.depth / textureW), 0),
            frontUVs: new BABYLON.Vector4(-1 + (minValueW), 0, 0 - (minValueW), minValueH),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        photoBottom.rotation.x = Math.PI * 1.5
        photoBottom.position.y = -this.wallart.height / 2
        photoBottom.position.z = this.wallart.depth / 2
        photoBottom.parent = wallartObj

        var photoLeft = BABYLON.MeshBuilder.CreatePlane('wallartLeft',
          {
            height: this.wallart.height,
            width: this.wallart.depth,
            updatable: true,
            frontUVs: new BABYLON.Vector4(0, minValueH, minValueW, maxValueH),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        photoLeft.rotation.y = -Math.PI * 1.5
        photoLeft.position.x = -this.wallart.width / 2
        photoLeft.position.z = this.wallart.depth / 2
        photoLeft.parent = wallartObj

        var photoRight = BABYLON.MeshBuilder.CreatePlane('wallartRight',
          {
            height: this.wallart.height,
            width: this.wallart.depth,
            updatable: true,
            frontUVs: new BABYLON.Vector4(maxValueW, minValueH, 1, maxValueH),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        photoRight.rotation.y = Math.PI * 1.5
        photoRight.position.x = this.wallart.width / 2
        photoRight.position.z = this.wallart.depth / 2
        photoRight.parent = wallartObj
      } else {
        minValueW = this.wallart.bleed / textureW
        maxValueW = 1 - (this.wallart.bleed / textureW)
        minValueH = this.wallart.bleed / textureH
        maxValueH = 1 - (this.wallart.bleed / textureH)

        photo = BABYLON.MeshBuilder.CreatePlane('photo',
          {
            height: this.wallart.height,
            width: this.wallart.width,
            updatable: true,
            frontUVs: new BABYLON.Vector4(minValueW, minValueH, maxValueW, maxValueH),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        photo.parent = wallartObj

        // if (this.wallart.type === 'acryl') {
        //   const acrylDepth = 3
        //   var acryl = BABYLON.MeshBuilder.CreateBox('acryl',
        //     {
        //       height: this.wallart.height,
        //       width: this.wallart.width,
        //       depth: acrylDepth,
        //       updatable: true,
        //       sideOrientation: BABYLON.Mesh.FRONTSIDE
        //     }, this.scene)
        //   acryl.position.z = -acrylDepth / 2
        //   acryl.parent = wallartObj
        // }
        var wallartBack = BABYLON.MeshBuilder.CreatePlane('wallartBack',
          {
            height: this.wallart.height,
            width: this.wallart.width,
            updatable: true,
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        wallartBack.rotation.y = Math.PI * 3
        wallartBack.position.z = this.wallart.depth
        wallartBack.parent = wallartObj

        var wallartTop = BABYLON.MeshBuilder.CreatePlane('wallartTop',
          {
            height: this.wallart.depth,
            width: this.wallart.width,
            updatable: true,
            frontUVs: new BABYLON.Vector4(0, 0, 1, 1),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        wallartTop.rotation.x = -Math.PI * 1.5
        wallartTop.position.y = this.wallart.height / 2
        wallartTop.position.z = this.wallart.depth / 2
        wallartTop.parent = wallartObj

        var wallartBottom = BABYLON.MeshBuilder.CreatePlane('wallartBottom',
          {
            height: this.wallart.depth,
            width: this.wallart.width,
            updatable: true,
            frontUVs: new BABYLON.Vector4(1, 1, 0, 0),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        wallartBottom.rotation.x = Math.PI * 1.5
        wallartBottom.position.y = -this.wallart.height / 2
        wallartBottom.position.z = this.wallart.depth / 2
        wallartBottom.parent = wallartObj

        var wallartLeft = BABYLON.MeshBuilder.CreatePlane('wallartLeft',
          {
            height: this.wallart.height,
            width: this.wallart.depth,
            updatable: true,
            frontUVs: new BABYLON.Vector4(1, 1, 0, 0),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        wallartLeft.rotation.y = -Math.PI * 1.5
        wallartLeft.position.x = -this.wallart.width / 2
        wallartLeft.position.z = this.wallart.depth / 2
        wallartLeft.parent = wallartObj

        var wallartRight = BABYLON.MeshBuilder.CreatePlane('wallartRight',
          {
            height: this.wallart.height,
            width: this.wallart.depth,
            updatable: true,
            frontUVs: new BABYLON.Vector4(0, 0, 1, 1),
            sideOrientation: BABYLON.Mesh.DOUBLESIDE
          }, this.scene)
        wallartRight.rotation.y = Math.PI * 1.5
        wallartRight.position.x = this.wallart.width / 2
        wallartRight.position.z = this.wallart.depth / 2
        wallartRight.parent = wallartObj
      }
      wallartObj.rotation.y = 200

      var photoCanvasShader = new BABYLON.PBRMaterial('photoCanvasShader', this.scene)
      photoCanvasShader.albedoColor = new BABYLON.Color3(1, 1, 1)
      photoCanvasShader.albedoTexture = new BABYLON.Texture('./../../static/wallartGenerator/aluwhite_baseTexture.jpg', this.scene)
      photoCanvasShader.ambientTexture = new BABYLON.Texture('./../../static/wallartGenerator/canvasAmbient.jpg', this.scene)
      photoCanvasShader.metallic = 0
      photoCanvasShader.roughness = 1

      var forexShader = new BABYLON.PBRMaterial('forexShader', this.scene)
      forexShader.albedoColor = new BABYLON.Color3(1, 1, 1)
      forexShader.albedoTexture = new BABYLON.Texture('./../../static/wallartGenerator/aluwhite_baseTexture.jpg', this.scene)
      forexShader.metallic = 0
      forexShader.roughness = 1

      var acrylShader = new BABYLON.PBRMaterial('acrylShader', this.scene)
      acrylShader.albedoColor = new BABYLON.Color3(1, 1, 1)
      acrylShader.albedoTexture = new BABYLON.Texture('./../../static/wallartGenerator/aluwhite_baseTexture.jpg', this.scene)
      acrylShader.metallic = 0
      acrylShader.roughness = 0

      var aluShader = new BABYLON.PBRMaterial('aluShader', this.scene)
      aluShader.albedoColor = new BABYLON.Color3(1, 1, 1)
      aluShader.albedoTexture = new BABYLON.Texture('./../../static/wallartGenerator/aluwhite_baseTexture.jpg', this.scene)
      aluShader.metallicTexture = new BABYLON.Texture('./../../static/wallartGenerator/alu_metallic.jpg', this.scene)
      aluShader.environmentIntensity = 1

      var aluWhiteShader = new BABYLON.PBRMaterial('aluWhiteShader', this.scene)
      aluWhiteShader.albedoColor = new BABYLON.Color3(1, 1, 1)
      aluWhiteShader.albedoTexture = new BABYLON.Texture('./../../static/wallartGenerator/aluwhite_baseTexture.jpg', this.scene)
      aluWhiteShader.useRoughnessFromMetallicTextureAlpha = false
      aluWhiteShader.useRoughnessFromMetallicTextureGreen = true
      aluWhiteShader.metallicTexture = new BABYLON.Texture('./../../static/wallartGenerator/aluwhite_metallicTexture.jpg', this.scene)
      aluWhiteShader.bumpTexture = new BABYLON.Texture('./../../static/wallartGenerator/aluwhite_normalTexture.png', this.scene)
      aluWhiteShader.environmentIntensity = 1

      // var probe = new BABYLON.ReflectionProbe('main', 512, this.scene)
      // var envTexture2 = new BABYLON.HDRCubeTexture('./../../static/textures/kitchen.hdr', this.scene, 256)
      // var acrylShader = new BABYLON.PBRMaterial('acrylShader', this.scene)
      // acrylShader.alpha = 0.1
      // acrylShader.reflectionTexture = envTexture2
      // acrylShader.useMicroSurfaceFromReflectivityMapAlpha = true
      // acrylShader.environmentIntensity = 1
      // acrylShader.specularIntensity = 0.3
      // acrylShader.reflectivityTexture = probe.cubeTexture

      var canvasShader = new BABYLON.PBRMaterial('canvasShader', this.scene)
      canvasShader.albedoColor = new BABYLON.Color3(1, 1, 1)
      canvasShader.ambientTexture = new BABYLON.Texture('./../../static/wallartGenerator/canvasAmbient.jpg', this.scene)
      canvasShader.metallic = 0
      canvasShader.roughness = 1
      canvasShader.microSurface = 0

      var wallartSideShader = new BABYLON.PBRMaterial('wallartSideShader', this.scene)
      wallartSideShader.albedoColor = new BABYLON.Color3(1, 1, 1)
      wallartSideShader.albedoTexture = new BABYLON.Texture('./../../static/wallartGenerator/wallartSide.jpg', this.scene)
      wallartSideShader.metallic = 0
      wallartSideShader.roughness = 1
      wallartSideShader.microSurface = 0

      var wallartSideShader2 = wallartSideShader.clone()
      wallartSideShader2.albedoTexture.wAng = Math.PI / 2

      if (this.wallart.type === 'canvas') {
        photo.material = photoCanvasShader
        photoTop.material = photoCanvasShader
        photoBottom.material = photoCanvasShader
        photoLeft.material = photoCanvasShader
        photoRight.material = photoCanvasShader
        photoBack.material = canvasShader
      } else {
        if (this.wallart.type === 'forex') photo.material = forexShader
        if (this.wallart.type === 'alu') photo.material = aluShader
        if (this.wallart.type === 'alu_white') photo.material = aluWhiteShader
        if (this.wallart.type === 'acryl') {
          photo.material = acrylShader
          // acryl.material = acrylShader
        }

        wallartTop.material = wallartSideShader
        wallartBottom.material = wallartSideShader
        wallartLeft.material = wallartSideShader2
        wallartRight.material = wallartSideShader2
      }

      if (this.firstLoad) {
        this.camera.target = wallartObj
        this.camera.radius = this.wallart.width >= this.wallart.height ? this.wallart.width * 2 : this.wallart.height * 2
        this.camera.alpha = -1.2
        this.camera.beta = Math.PI / 2
        this.camera.lowerAlphaLimit = -Math.PI / 1.5
        this.camera.upperAlphaLimit = Math.PI / 2.92
        this.firstLoad = false
      }
      this.camera.wheelPrecision = ((1 / this.wallart.width) * 100) + 0.5
    },
    applyWallartScene (envKey, envRes) {
      var scene = new BABYLON.Scene(this.engine)
      var camera = new BABYLON.ArcRotateCamera('Camera', -Math.PI / 1, Math.PI / 3.5, 10, BABYLON.Vector3.Zero(), scene)
      camera.attachControl(this.canvas, true)

      this.applyEnv(scene, envKey, envRes)

      this.camera = camera
      this.scene = scene
    }
  }
}
