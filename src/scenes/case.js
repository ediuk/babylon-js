import * as BABYLON from 'babylonjs'
import 'babylonjs-loaders'

export const caseScene = {
  data () {
    return {
      case: {
        photoNum: 0
      }
    }
  },
  methods: {
    applyCaseScene (envKey, envRes) {
      var scene = new BABYLON.Scene(this.engine)
      var camera = new BABYLON.ArcRotateCamera('Camera', -Math.PI / 1, Math.PI / 3.5, 10, BABYLON.Vector3.Zero(), scene)
      camera.useAutoRotationBehavior = true
      camera.autoRotationBehavior.idleRotationSpeed = 0.2
      camera.autoRotationBehavior.idleRotationWaitTime = 1000
      camera.autoRotationBehavior.idleRotationSpinupTime = 2000
      camera.autoRotationBehavior.zoomStopsAnimation = true
      camera.wheelPrecision = 50
      camera.attachControl(this.canvas, true)

      var pbr1 = new BABYLON.PBRMaterial('pbr', scene)
      pbr1.albedoColor = new BABYLON.Color3(1, 1, 1)
      pbr1.metallic = 0.1
      pbr1.roughness = 0.5
      pbr1.microSurface = 0
      pbr1.backFaceCulling = false

      var pbr2 = new BABYLON.PBRMaterial('pbr2', scene)
      pbr2.albedoTexture = new BABYLON.Texture('./../../static/case/photo0.jpg', scene)
      pbr2.albedoTexture.vScale = -1
      pbr2.albedoColor = new BABYLON.Color3(1, 1, 1)
      pbr2.metallic = 0.3
      pbr2.roughness = 0
      pbr2.microSurface = 0
      pbr2.backFaceCulling = false

      var multimat = new BABYLON.MultiMaterial('multi', scene)
      multimat.subMaterials.push(pbr1)
      multimat.subMaterials.push(pbr2)

      var that = this

      BABYLON.SceneLoader.ImportMesh('', './../../static/case/', 'case.gltf', scene, function (newMeshes) {
        newMeshes[1].material = pbr1
        newMeshes[1].scaling.y = 0.01
        newMeshes[1].scaling.x = 0.01
        newMeshes[1].scaling.z = 0.01

        newMeshes[2].material = pbr2
        newMeshes[2].scaling.y = 0.01
        newMeshes[2].scaling.x = 0.01
        newMeshes[2].scaling.z = 0.01

        camera.setTarget(new BABYLON.Vector3(newMeshes[1].position.x - 6, newMeshes[1].position.y, newMeshes[1].position.z))
        camera.radius = 10
        camera.beta = Math.PI / 2.5
        camera.alpha = -Math.PI / 0.7

        that.applyEnv(scene, envKey, envRes)
      })
      this.scene = scene
    }
  }
}
