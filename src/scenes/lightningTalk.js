import * as BABYLON from 'babylonjs'
import 'babylonjs-loaders'

export const lightningTalkScene = {
  data () {
    return {
      loaded: false
    }
  },
  methods: {
    applyLightningTalkScene () {
      var scene = new BABYLON.Scene(this.engine)
      var camera = new BABYLON.ArcRotateCamera('Camera', -Math.PI / 1, Math.PI / 3.5, 10, BABYLON.Vector3.Zero(), scene)

      camera.useAutoRotationBehavior = true
      camera.autoRotationBehavior.idleRotationSpeed = 0.2
      camera.autoRotationBehavior.idleRotationWaitTime = 1000
      camera.autoRotationBehavior.idleRotationSpinupTime = 2000
      camera.autoRotationBehavior.zoomStopsAnimation = false

      // camera.attachControl(this.canvas, true)
      camera.radius = 60
      camera.beta = Math.PI / 2
      camera.alpha = -Math.PI / 2

      var that = this
      this.loaded = false
      BABYLON.SceneLoader.ImportMesh('', './../../static/lightningTalk/', 'lightningTalk.obj', scene, function (newMeshes) {
        that.loaded = true
      })
      this.scene = scene
    }
  },
  watch: {
    'loaded': function () {
      this.scene.imageProcessingConfiguration.exposure = 1
      this.scene.imageProcessingConfiguration.contrast = 1
      // this.scene.createDefaultEnvironment()

      var pbr = new BABYLON.PBRMaterial('pbr', this.scene)
      pbr.albedoColor = new BABYLON.Color3(1, 1, 1)
      pbr.metallic = 0.8
      pbr.roughness = 0.1

      var pbr3 = new BABYLON.PBRMaterial('pbr3', this.scene)
      pbr3.albedoColor = new BABYLON.Color3(1, 1, 1)
      pbr.microSurface = 0.5

      this.scene.meshes[0].material = pbr
      this.scene.meshes[1].material = pbr
      this.scene.meshes[2].material = pbr
      this.scene.meshes[3].material = pbr

      // Animation
      var frameRate = 100
      var zRot1 = new BABYLON.Animation('zRot1', 'rotation.z', frameRate, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE)
      var keyFramesR1 = []
      keyFramesR1.push({ frame: 0, value: 0 })
      keyFramesR1.push({ frame: frameRate, value: 2 * Math.PI })
      zRot1.setKeys(keyFramesR1)

      var envTexture = new BABYLON.HDRCubeTexture('./../../static/textures/beach.hdr', this.scene, 512)
      envTexture.level = 0.5
      this.scene.createDefaultSkybox(envTexture, true, 1000, 10)

      this.scene.meshes[0].animations = []
      this.scene.meshes[0].animations.push(zRot1)
      this.scene.meshes[1].animations = []
      this.scene.meshes[1].animations.push(zRot1)
      this.scene.meshes[2].animations = []
      this.scene.meshes[2].animations.push(zRot1)
      this.scene.beginAnimation(this.scene.meshes[0], 0, frameRate, true, 0.05)
      this.scene.beginAnimation(this.scene.meshes[1], 0, frameRate, true, 0.2)
      this.scene.beginAnimation(this.scene.meshes[2], 0, frameRate, true, 0.1)
    }
  }
}
