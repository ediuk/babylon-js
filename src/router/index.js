import Vue from 'vue'
import Router from 'vue-router'

import BabylonJS from '@/components/BabylonJS'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'BabylonJS',
      component: BabylonJS
    }
  ]
})
